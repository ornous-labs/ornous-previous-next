import React from 'react'
import styled from '@emotion/styled'

const Layout = styled.div`
  margin: 10%;
  background-color: #fafafa;
`

export default ({ children }) => (
  <Layout>
    {children}
  </Layout>
)
