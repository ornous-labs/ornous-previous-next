module.exports = {
  plugins: ['babel-plugin-emotion'],
  presets: ['@babel/preset-env', '@babel/preset-react']
}
