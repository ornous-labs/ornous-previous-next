import React from 'react'
import { shallow } from 'enzyme'

import { Card } from '..'

describe('Card', () => {
  it('should be defined', () => {
    expect(Card).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = shallow(<Card>Label</Card>)

    expect(tree).toMatchSnapshot()
  })
})
