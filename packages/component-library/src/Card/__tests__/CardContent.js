import React from 'react'
import { shallow } from 'enzyme'

import { CardContent } from '..'

describe('CardContent', () => {
  it('should be defined', () => {
    expect(CardContent).toBeDefined()
  })

  it('should render correctly', () => {
    const tree = shallow(<CardContent>Label</CardContent>)

    expect(tree).toMatchSnapshot()
  })
})
