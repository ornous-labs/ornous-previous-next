import React from 'react'
import styled from '@emotion/styled'

import Button from '../Button'

const IconButton = styled(Button)`
  transform: scale(1.34);
  margin: 0;
  padding: 0 0.16rem;
  color: var(--primary-color, black);
  background-color: transparent;
  border: none;
`

export default ({ className, toggle }) => (
  <IconButton type="button" className={className} onClick={toggle}>
    x
  </IconButton>
)
