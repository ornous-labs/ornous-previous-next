import React from 'react'
import styled from '@emotion/styled'

import useModal from './useModal'
import Modal from './Modal'
import Button from '../Button'

const Trigger = styled(Button)`
  border: 2px solid #bad2fa;
  border-radius: 1em;

  &:focus {
    box-shadow: 0 0 4px currentColor;
  }
`

const Reveal = ({ className, ctaLabel, children }) => {
  const { isShowing, toggle, target } = useModal()

  return (
    <>
      <Trigger classname={className} onClick={toggle} >{ctaLabel}</Trigger>

      {isShowing && (
        <Modal target={target} toggle={toggle}>{children}</Modal>
      )}
    </>
  )
}

export default Reveal
