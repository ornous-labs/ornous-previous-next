import React, { useRef } from 'react'
import { createPortal } from 'react-dom'

import Backdrop from './Backdrop'
import Stage from './Stage'
import CloseButton from './CloseButton'
import Contents from './Contents'

export default ({ target, toggle, children }) => {
  const containerRef = useRef(null)

  return createPortal(
    <>
      <Backdrop onClick={(e) => {
        // Close on click outside
        if (!containerRef.current.contains(e.target)) {
          toggle()
        }
      }}>
        <Stage ref={containerRef}>
          <CloseButton onClick={toggle} />
          <Contents>{children}</Contents>
        </Stage>
      </Backdrop>
    </>,
    target
  )
}
