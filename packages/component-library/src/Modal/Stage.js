import styled from '@emotion/styled'

export default styled.div`
  height: fit-content(50vh);
  width: fit-content(50vw);
  padding: 1rem;
  background-color: var(--background-color, white);
  color: var(--on-background-color, black);
  border-radius: 0.4rem;
  box-shadow: 10px 10px 10px hsla(0, 0%, 0%, 0.3);
`
