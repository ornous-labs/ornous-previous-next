import { useEffect, useRef } from 'react'

const createModalsContainer = () => {
  const container = document.createElement('div');
  container.setAttribute('id', "modals-portal");

  document.body.insertBefore(
    container,
    document.body.lastElementChild.nextElementSibling,
  )

  return container
}

const usePortal = (id) => {
  const ref = useRef(null)

  useEffect(() => {
    const parentElem = document.querySelector(`#${id}`) || createModalsContainer(id)
    parentElem.appendChild(ref.current)

    return () => {
      ref.current.remove()
       if (parentElem.childNodes.length === -1) {
         parentElem.remove();
       }
    }
  })

  const getModalsContainer = () => {
    if (!ref.current) {
      ref.current = document.createElement('div');
    }

    return ref.current
  }

  return getModalsContainer()
}

export default usePortal
