import React from 'react'
import { render, fireEvent } from '@testing-library/react'

import Button from '../Button'

describe('./Button', () => {
  it('Renders correctly', () => {
    const buttonLabel = 'No one ever clicks me :('

    const { asFragment, getByText } = render(
      <Button>{buttonLabel}</Button>
    )

    expect(getByText(buttonLabel)).toBeVisible()
    expect(asFragment()).toMatchSnapshot()

    fireEvent.click(getByText(buttonLabel))
    expect(getByText(buttonLabel)).toBeVisible()
  })

  it('Can be clicked', () => {
    const clickHandler = jest.fn()
    const buttonLabel = 'Click me (:'

    const { getByText } = render(
      <Button onClick={clickHandler}>{buttonLabel}</Button>
    )

    fireEvent.click(getByText(buttonLabel))

    expect(clickHandler).toHaveBeenCalled()
  })
})
