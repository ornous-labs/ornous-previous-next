"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

var _default = (0, _styledBase["default"])("div", {
  target: "e3q779q0"
})(process.env.NODE_ENV === "production" ? {
  name: "1583q6f",
  styles: "height:fit-content(50vh);width:fit-content(50vw);padding:1rem;background-color:var(--background-color,white);color:var(--on-background-color,black);border-radius:0.4rem;box-shadow:10px 10px 10px hsla(0,0%,0%,0.3);"
} : {
  name: "1583q6f",
  styles: "height:fit-content(50vh);width:fit-content(50vw);padding:1rem;background-color:var(--background-color,white);color:var(--on-background-color,black);border-radius:0.4rem;box-shadow:10px 10px 10px hsla(0,0%,0%,0.3);",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9Nb2RhbC9TdGFnZS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFeUIiLCJmaWxlIjoiLi4vLi4vc3JjL01vZGFsL1N0YWdlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHN0eWxlZCBmcm9tICdAZW1vdGlvbi9zdHlsZWQnXG5cbmV4cG9ydCBkZWZhdWx0IHN0eWxlZC5kaXZgXG4gIGhlaWdodDogZml0LWNvbnRlbnQoNTB2aCk7XG4gIHdpZHRoOiBmaXQtY29udGVudCg1MHZ3KTtcbiAgcGFkZGluZzogMXJlbTtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tYmFja2dyb3VuZC1jb2xvciwgd2hpdGUpO1xuICBjb2xvcjogdmFyKC0tb24tYmFja2dyb3VuZC1jb2xvciwgYmxhY2spO1xuICBib3JkZXItcmFkaXVzOiAwLjRyZW07XG4gIGJveC1zaGFkb3c6IDEwcHggMTBweCAxMHB4IGhzbGEoMCwgMCUsIDAlLCAwLjMpO1xuYFxuIl19 */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

exports["default"] = _default;