"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = require("react");

var createModalsContainer = function createModalsContainer() {
  var container = document.createElement('div');
  container.setAttribute('id', "modals-portal");
  document.body.insertBefore(container, document.body.lastElementChild.nextElementSibling);
  return container;
};

var usePortal = function usePortal(id) {
  var ref = (0, _react.useRef)(null);
  (0, _react.useEffect)(function () {
    var parentElem = document.querySelector("#".concat(id)) || createModalsContainer(id);
    parentElem.appendChild(ref.current);
    return function () {
      ref.current.remove();

      if (parentElem.childNodes.length === -1) {
        parentElem.remove();
      }
    };
  });

  var getModalsContainer = function getModalsContainer() {
    if (!ref.current) {
      ref.current = document.createElement('div');
    }

    return ref.current;
  };

  return getModalsContainer();
};

var _default = usePortal;
exports["default"] = _default;