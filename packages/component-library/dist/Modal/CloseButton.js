"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

var _react = _interopRequireDefault(require("react"));

var _Button = _interopRequireDefault(require("../Button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

var IconButton = ( /*#__PURE__*/0, _styledBase["default"])(_Button["default"], {
  target: "e1dgwx530",
  label: "IconButton"
})(process.env.NODE_ENV === "production" ? {
  name: "1mau3vi",
  styles: "transform:scale(1.34);margin:0;padding:0 0.16rem;color:var(--primary-color,black);background-color:transparent;border:none;"
} : {
  name: "1mau3vi",
  styles: "transform:scale(1.34);margin:0;padding:0 0.16rem;color:var(--primary-color,black);background-color:transparent;border:none;",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9Nb2RhbC9DbG9zZUJ1dHRvbi5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLaUMiLCJmaWxlIjoiLi4vLi4vc3JjL01vZGFsL0Nsb3NlQnV0dG9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xuaW1wb3J0IHN0eWxlZCBmcm9tICdAZW1vdGlvbi9zdHlsZWQnXG5cbmltcG9ydCBCdXR0b24gZnJvbSAnLi4vQnV0dG9uJ1xuXG5jb25zdCBJY29uQnV0dG9uID0gc3R5bGVkKEJ1dHRvbilgXG4gIHRyYW5zZm9ybTogc2NhbGUoMS4zNCk7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMCAwLjE2cmVtO1xuICBjb2xvcjogdmFyKC0tcHJpbWFyeS1jb2xvciwgYmxhY2spO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyOiBub25lO1xuYFxuXG5leHBvcnQgZGVmYXVsdCAoeyBjbGFzc05hbWUsIHRvZ2dsZSB9KSA9PiAoXG4gIDxJY29uQnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzc05hbWU9e2NsYXNzTmFtZX0gb25DbGljaz17dG9nZ2xlfT5cbiAgICB4XG4gIDwvSWNvbkJ1dHRvbj5cbilcbiJdfQ== */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

var _default = function _default(_ref) {
  var className = _ref.className,
      toggle = _ref.toggle;
  return /*#__PURE__*/_react["default"].createElement(IconButton, {
    type: "button",
    className: className,
    onClick: toggle
  }, "x");
};

exports["default"] = _default;