"use strict";

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@testing-library/react");

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// import { getByText as domGetByText } from '@testing-library/dom'
describe('RevealModal', function () {
  beforeEach(function () {
    var modalsPortal = document.createElement('div');
    modalsPortal.setAttribute('id', 'modals-portal');
    document.body.appendChild(modalsPortal);
  });
  it('shows modal content on click', function () {
    var children = ["Tom", "Meena", "Abdoul", "Takako"].join(', ');

    var _render = (0, _react2.render)( /*#__PURE__*/_react["default"].createElement(_.RevealModal, {
      revealLabel: "See Children"
    }, children)),
        getByText = _render.getByText;

    expect(getByText('See Children')).toBeVisible(); // (domGetByText(document, children)).toBeNotVisible()

    _react2.fireEvent.click(getByText('See Children'));

    expect(getByText(children)).toBeVisible();
  });
  describe('an open modal', function () {
    var wrapper;
    var kids = "They are alright";
    beforeEach(function () {
      wrapper = (0, _react2.render)( /*#__PURE__*/_react["default"].createElement(_.RevealModal, {
        revealLabel: "How are the kids"
      }, kids));

      _react2.fireEvent.click(wrapper.getByText("How are the kids"));
    });
    it('can be closed', function () {
      var _wrapper = wrapper,
          getByText = _wrapper.getByText;
      expect(getByText(kids)).toBeVisible();
    });
  });
});