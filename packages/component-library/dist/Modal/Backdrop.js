"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

var _default = (0, _styledBase["default"])("aside", {
  target: "e1uhixoe0"
})(process.env.NODE_ENV === "production" ? {
  name: "k68th6",
  styles: "position:fixed;top:0;left:0;bottom:0;right:0;background-color:hsla(0,0%,0%,0.8);z-index:1;width:100vw;height:100vh;display:flex;align-items:center;justify-content:center;"
} : {
  name: "k68th6",
  styles: "position:fixed;top:0;left:0;bottom:0;right:0;background-color:hsla(0,0%,0%,0.8);z-index:1;width:100vw;height:100vh;display:flex;align-items:center;justify-content:center;",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9Nb2RhbC9CYWNrZHJvcC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFMkIiLCJmaWxlIjoiLi4vLi4vc3JjL01vZGFsL0JhY2tkcm9wLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHN0eWxlZCBmcm9tICdAZW1vdGlvbi9zdHlsZWQnXG5cbmV4cG9ydCBkZWZhdWx0IHN0eWxlZC5hc2lkZWBcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIGJvdHRvbTogMDtcbiAgcmlnaHQ6IDA7XG4gIGJhY2tncm91bmQtY29sb3I6IGhzbGEoMCwgMCUsIDAlLCAwLjgpO1xuICB6LWluZGV4OiAxO1xuICB3aWR0aDogMTAwdnc7XG4gIGhlaWdodDogMTAwdmg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuYFxuIl19 */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

exports["default"] = _default;