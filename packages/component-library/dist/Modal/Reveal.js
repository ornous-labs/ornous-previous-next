"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

var _react = _interopRequireDefault(require("react"));

var _useModal2 = _interopRequireDefault(require("./useModal"));

var _Modal = _interopRequireDefault(require("./Modal"));

var _Button = _interopRequireDefault(require("../Button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

var Trigger = ( /*#__PURE__*/0, _styledBase["default"])(_Button["default"], {
  target: "e17ds7xs0",
  label: "Trigger"
})(process.env.NODE_ENV === "production" ? {
  name: "mjyrzk",
  styles: "border:2px solid #bad2fa;border-radius:1em;&:focus{box-shadow:0 0 4px currentColor;}"
} : {
  name: "mjyrzk",
  styles: "border:2px solid #bad2fa;border-radius:1em;&:focus{box-shadow:0 0 4px currentColor;}",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9Nb2RhbC9SZXZlYWwuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBTzhCIiwiZmlsZSI6Ii4uLy4uL3NyYy9Nb2RhbC9SZXZlYWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXG5pbXBvcnQgc3R5bGVkIGZyb20gJ0BlbW90aW9uL3N0eWxlZCdcblxuaW1wb3J0IHVzZU1vZGFsIGZyb20gJy4vdXNlTW9kYWwnXG5pbXBvcnQgTW9kYWwgZnJvbSAnLi9Nb2RhbCdcbmltcG9ydCBCdXR0b24gZnJvbSAnLi4vQnV0dG9uJ1xuXG5jb25zdCBUcmlnZ2VyID0gc3R5bGVkKEJ1dHRvbilgXG4gIGJvcmRlcjogMnB4IHNvbGlkICNiYWQyZmE7XG4gIGJvcmRlci1yYWRpdXM6IDFlbTtcblxuICAmOmZvY3VzIHtcbiAgICBib3gtc2hhZG93OiAwIDAgNHB4IGN1cnJlbnRDb2xvcjtcbiAgfVxuYFxuXG5jb25zdCBSZXZlYWwgPSAoeyBjbGFzc05hbWUsIGN0YUxhYmVsLCBjaGlsZHJlbiB9KSA9PiB7XG4gIGNvbnN0IHsgaXNTaG93aW5nLCB0b2dnbGUsIHRhcmdldCB9ID0gdXNlTW9kYWwoKVxuXG4gIHJldHVybiAoXG4gICAgPD5cbiAgICAgIDxUcmlnZ2VyIGNsYXNzbmFtZT17Y2xhc3NOYW1lfSBvbkNsaWNrPXt0b2dnbGV9ID57Y3RhTGFiZWx9PC9UcmlnZ2VyPlxuXG4gICAgICB7aXNTaG93aW5nICYmIChcbiAgICAgICAgPE1vZGFsIHRhcmdldD17dGFyZ2V0fSB0b2dnbGU9e3RvZ2dsZX0+e2NoaWxkcmVufTwvTW9kYWw+XG4gICAgICApfVxuICAgIDwvPlxuICApXG59XG5cbmV4cG9ydCBkZWZhdWx0IFJldmVhbFxuIl19 */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

var Reveal = function Reveal(_ref) {
  var className = _ref.className,
      ctaLabel = _ref.ctaLabel,
      children = _ref.children;

  var _useModal = (0, _useModal2["default"])(),
      isShowing = _useModal.isShowing,
      toggle = _useModal.toggle,
      target = _useModal.target;

  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(Trigger, {
    classname: className,
    onClick: toggle
  }, ctaLabel), isShowing && /*#__PURE__*/_react["default"].createElement(_Modal["default"], {
    target: target,
    toggle: toggle
  }, children));
};

var _default = Reveal;
exports["default"] = _default;