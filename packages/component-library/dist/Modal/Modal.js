"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = require("react-dom");

var _Backdrop = _interopRequireDefault(require("./Backdrop"));

var _Stage = _interopRequireDefault(require("./Stage"));

var _CloseButton = _interopRequireDefault(require("./CloseButton"));

var _Contents = _interopRequireDefault(require("./Contents"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var _default = function _default(_ref) {
  var target = _ref.target,
      toggle = _ref.toggle,
      children = _ref.children;
  var containerRef = (0, _react.useRef)(null);
  return (0, _reactDom.createPortal)( /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_Backdrop["default"], {
    onClick: function onClick(e) {
      // Close on click outside
      if (!containerRef.current.contains(e.target)) {
        toggle();
      }
    }
  }, /*#__PURE__*/_react["default"].createElement(_Stage["default"], {
    ref: containerRef
  }, /*#__PURE__*/_react["default"].createElement(_CloseButton["default"], {
    onClick: toggle
  }), /*#__PURE__*/_react["default"].createElement(_Contents["default"], null, children)))), target);
};

exports["default"] = _default;