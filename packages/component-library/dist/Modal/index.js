"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _Modal["default"];
  }
});
Object.defineProperty(exports, "RevealModal", {
  enumerable: true,
  get: function get() {
    return _Reveal["default"];
  }
});

var _Modal = _interopRequireDefault(require("./Modal"));

var _Reveal = _interopRequireDefault(require("./Reveal"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }