"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

var Layout = (0, _styledBase["default"])("div", {
  target: "eyrs7wh0",
  label: "Layout"
})(process.env.NODE_ENV === "production" ? {
  name: "1q2cnc2",
  styles: "margin:10%;background-color:#fafafa;"
} : {
  name: "1q2cnc2",
  styles: "margin:10%;background-color:#fafafa;",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9sYXlvdXQuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBR3lCIiwiZmlsZSI6Ii4uL3NyYy9sYXlvdXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnXG5pbXBvcnQgc3R5bGVkIGZyb20gJ0BlbW90aW9uL3N0eWxlZCdcblxuY29uc3QgTGF5b3V0ID0gc3R5bGVkLmRpdmBcbiAgbWFyZ2luOiAxMCU7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmYWZhZmE7XG5gXG5cbmV4cG9ydCBkZWZhdWx0ICh7IGNoaWxkcmVuIH0pID0+IChcbiAgPExheW91dD5cbiAgICB7Y2hpbGRyZW59XG4gIDwvTGF5b3V0PlxuKVxuIl19 */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

var _default = function _default(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/_react["default"].createElement(Layout, null, children);
};

exports["default"] = _default;