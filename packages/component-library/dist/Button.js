"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _styledBase = _interopRequireDefault(require("@emotion/styled-base"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _EMOTION_STRINGIFIED_CSS_ERROR__() { return "You have tried to stringify object returned from `css` function. It isn't supposed to be used directly (e.g. as value of the `className` prop), but rather handed to emotion so it can handle it (e.g. as value of `css` prop)."; }

var _default = (0, _styledBase["default"])("button", {
  target: "ebo35to0"
})(process.env.NODE_ENV === "production" ? {
  name: "1558mcz",
  styles: "padding:.1em .5em;color:#4285f4;background:#e2edff;border:1px solid #bad2fa;border-radius:.5em;cursor:pointer;transition:background .15s ease-out;&:hover{background:#cbddfb;}&:focus{outline:0;box-shadow:0 0 4px currentColor;}"
} : {
  name: "1558mcz",
  styles: "padding:.1em .5em;color:#4285f4;background:#e2edff;border:1px solid #bad2fa;border-radius:.5em;cursor:pointer;transition:background .15s ease-out;&:hover{background:#cbddfb;}&:focus{outline:0;box-shadow:0 0 4px currentColor;}",
  map: "/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9CdXR0b24uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRTRCIiwiZmlsZSI6Ii4uL3NyYy9CdXR0b24uanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgc3R5bGVkIGZyb20gJ0BlbW90aW9uL3N0eWxlZCdcblxuZXhwb3J0IGRlZmF1bHQgc3R5bGVkLmJ1dHRvbmBcbiAgcGFkZGluZzogLjFlbSAuNWVtO1xuICBjb2xvcjogIzQyODVmNDtcbiAgYmFja2dyb3VuZDogI2UyZWRmZjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2JhZDJmYTtcbiAgYm9yZGVyLXJhZGl1czogLjVlbTtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIC4xNXMgZWFzZS1vdXQ7XG5cbiAgJjpob3ZlciB7XG4gICAgYmFja2dyb3VuZDogI2NiZGRmYjtcbiAgfVxuXG4gICY6Zm9jdXMge1xuICAgIG91dGxpbmU6IDA7XG4gICAgYm94LXNoYWRvdzogMCAwIDRweCBjdXJyZW50Q29sb3I7XG4gIH1cbmBcbiJdfQ== */",
  toString: _EMOTION_STRINGIFIED_CSS_ERROR__
});

exports["default"] = _default;