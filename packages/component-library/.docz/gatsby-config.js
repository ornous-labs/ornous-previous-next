const { mergeWith } = require('lodash/fp')
const fs = require('fs-extra')

let custom = {}
const hasGatsbyConfig = fs.existsSync('./gatsby-config.custom.js')

if (hasGatsbyConfig) {
  try {
    custom = require('./gatsby-config.custom')
  } catch (err) {
    console.error(
      `Failed to load your gatsby-config.js file : `,
      JSON.stringify(err),
    )
  }
}

const config = {
  pathPrefix: '/',

  siteMetadata: {
    title: 'Component Library',
    description: 'My awesome app using docz',
  },
  plugins: [
    {
      resolve: 'gatsby-theme-docz',
      options: {
        themeConfig: {},
        themesDir: 'src',
        mdxExtensions: ['.md', '.mdx'],
        docgenConfig: {},
        menu: [],
        mdPlugins: [],
        hastPlugins: [],
        ignore: [],
        typescript: false,
        ts: false,
        propsParser: true,
        'props-parser': true,
        debug: false,
        native: false,
        openBrowser: false,
        o: false,
        open: false,
        'open-browser': false,
        root:
          '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz',
        base: '/',
        source: './',
        src: './',
        files: '**/*.{md,markdown,mdx}',
        public: '/public',
        dest: '.docz/dist',
        d: '.docz/dist',
        editBranch: 'master',
        eb: 'master',
        'edit-branch': 'master',
        config: '',
        title: 'Component Library',
        description: 'My awesome app using docz',
        host: 'localhost',
        port: 3000,
        p: 3000,
        separator: '-',
        paths: {
          root:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library',
          templates:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/node_modules/docz-core/dist/templates',
          docz:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz',
          cache:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz/.cache',
          app:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz/app',
          appPackageJson:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/package.json',
          gatsbyConfig:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/gatsby-config.js',
          gatsbyBrowser:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/gatsby-browser.js',
          gatsbyNode:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/gatsby-node.js',
          gatsbySSR:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/gatsby-ssr.js',
          importsJs:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz/app/imports.js',
          rootJs:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz/app/root.jsx',
          indexJs:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz/app/index.jsx',
          indexHtml:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz/app/index.html',
          db:
            '/Users/ozzy/Projects/lab/experiments/yarn-lerna-monorepo/packages/component-library/.docz/app/db.json',
        },
      },
    },
  ],
}

const merge = mergeWith((objValue, srcValue) => {
  if (Array.isArray(objValue)) {
    return objValue.concat(srcValue)
  }
})

module.exports = merge(config, custom)
